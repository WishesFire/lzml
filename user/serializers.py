from rest_framework_simplejwt.tokens import RefreshToken, TokenError
from django.contrib.auth.password_validation import validate_password
from rest_framework.exceptions import AuthenticationFailed
from rest_framework import serializers
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.utils.encoding import force_str
from django.utils.http import urlsafe_base64_decode
from .models import User


class UserSerializer(serializers.ModelSerializer):
    """
    Validate registration data from creating new user
    """
    email = serializers.EmailField(max_length=255, min_length=4)
    password = serializers.CharField(max_length=65, min_length=8, write_only=True, validators=[validate_password])
    password2 = serializers.CharField(max_length=65, min_length=8, write_only=True)

    class Meta:
        model = User
        fields = ("id", "email", "password", "password2")
        extra_kwargs = {
            "password": {"write_only": True}
        }

    def validate(self, attrs):
        email = attrs.get('email', None)
        password = attrs.get("password", None)
        password2 = attrs.get("password2", None)
        if email is not None and password is not None and password2 is not None:
            if User.objects.filter(email=email).exists():
                raise serializers.ValidationError({'email': 'Email is already in use'})
            if password != password2:
                raise serializers.ValidationError({'password': 'Password1 is not similar password2'})
            return super().validate(attrs)
        raise serializers.ValidationError({"msg_error": "Not all data is available"})

    def create(self, validated_data):
        user = User.objects.create_user(email=validated_data['email'], password=validated_data['password'])
        user.save()

        return user

    def update(self, instance, validated_data):
        password = validated_data.pop("password", None)
        for key, value in validated_data.items():
            setattr(instance, key, value)
        if password is not None:
            instance.set_password(password)
        instance.save()

        return instance


class LoginSerializer(serializers.Serializer):
    """
    Validate login data from existing user
    """
    email = serializers.EmailField(max_length=255, min_length=4)
    password = serializers.CharField(max_length=65, min_length=8, write_only=True)
    tokens = serializers.SerializerMethodField()

    @staticmethod
    def get_tokens(obj):
        user = User.objects.get(email=obj['email'])

        return {
            'refresh': user.tokens()['refresh'],
            'access': user.tokens()['access']
        }

    def validate(self, attrs):
        validated_data = super().validate(attrs)
        email = validated_data['email']
        password = validated_data['password']
        try:
            user = User.objects.filter(email=email).first()
            if user is None:
                raise AuthenticationFailed("User not found")
            if not user.check_password(password):
                raise AuthenticationFailed('email or password are incorrect')
            validated_data["user"] = user
        except Exception as _:
            raise serializers.ValidationError('email or password are incorrect')

        return {
            'email': user.email,
            'username': user.username,
            'tokens': user.tokens
        }

    def create(self, validated_data):
        pass

    def update(self, instance, validated_data):
        pass


class LogoutSerializer(serializers.Serializer):
    refresh = serializers.CharField(required=True, write_only=True)

    def validate(self, attrs):
        self.token = attrs["refresh"]
        return attrs

    def save(self, **kwargs):
        try:
            RefreshToken(self.token).blacklist()
        except TokenError:
            self.fail('bad_token')

    def create(self, validated_data):
        pass

    def update(self, instance, validated_data):
        pass


class ResetPasswordEmailSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(required=True, write_only=True)
    redirect_url = serializers.CharField(max_length=500, required=False)

    class Meta:
        fields = ("email", )


class SetNewPasswordSerializer(serializers.ModelSerializer):
    password = serializers.CharField(max_length=65, min_length=8, write_only=True)
    token = serializers.CharField(write_only=True)
    ui_db64 = serializers.CharField(write_only=True)

    class Meta:
        fields = ("password", "token", "ui_db64")

    def validate(self, attrs):
        try:
            password = attrs.get('password')
            token = attrs.get('token')
            ui_db64 = attrs.get('ui_db64')
            user_id = force_str(urlsafe_base64_decode(ui_db64))
            user = User.objects.get(id=user_id)

            if not PasswordResetTokenGenerator().check_token(user, token):
                raise AuthenticationFailed('The reset link is invalid', 401)

            user.set_password(password)
            user.save()

            return user

        except Exception as _:
            raise AuthenticationFailed('The reset link is invalid', 401)
