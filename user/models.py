from django.db import models
from rest_framework_simplejwt.tokens import RefreshToken
from django.contrib.auth.models import AbstractUser, PermissionsMixin
from .services.user_manager import UserManager


class User(AbstractUser, PermissionsMixin):
    """
    User model on the platform
    """
    username = None
    email = models.EmailField(max_length=150, unique=True)
    password = models.CharField(max_length=200)
    join_date = models.DateTimeField(auto_now_add=True)
    is_admin = models.BooleanField('admin', default=False)
    ban_status = models.BooleanField(default=False)

    objects = UserManager()

    REQUIRED_FIELDS = []
    USERNAME_FIELD = 'email'

    @property
    def is_authenticated(self):
        return True

    def tokens(self):
        refresh = RefreshToken.for_user(self)
        return {
            "access": str(refresh.access_token),
            "refresh": str(refresh)
        }

    def __str__(self):
        return self.email
