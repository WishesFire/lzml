import jwt
from rest_framework import authentication, exceptions
from forum.settings import JWT_SECRET
from .models import User


class JWTAuthentication(authentication.BaseAuthentication):
    def authenticate(self, request):
        auth_data = authentication.get_authorization_header(request)
        if not auth_data:
            return None

        prefix, token = auth_data.decode("utf-8").split(" ")
        try:
            payload = jwt.decode(token, JWT_SECRET, algorithms="HS256")
            user = User.objects.get(email=payload["email"])

            return user, token

        except jwt.DecodeError as _:
            raise exceptions.AuthenticationFailed('Your token is invalid,login')
        except jwt.ExpiredSignatureError as _:
            raise exceptions.AuthenticationFailed('Your token is expired,login')
        finally:
            return super().authenticate(request)
