from django.urls import path
from .views import RegisterView, LoginView, LogoutView, \
    PasswordResetEmail, PasswordTokenCheck, SetNewPassword
from rest_framework_simplejwt.views import TokenRefreshView

app_name = 'authentication'

urlpatterns = [
    path("register/", RegisterView.as_view(), name="register"),
    path("login/", LoginView.as_view(), name="login"),
    path("logout/", LogoutView.as_view(), name="logout"),
    path('refresh/', TokenRefreshView.as_view(), name='token_refresh'),

    path("reset-email/", PasswordResetEmail.as_view(), name="request-reset-email"),
    path("password-reset/<uidb64>/<token>/", PasswordTokenCheck.as_view(), name="password-reset-confirm"),
    path("password-reset-complete", SetNewPassword.as_view(), name="password-reset-complete")
]
