import uuid
import os
from rest_framework import status, permissions, generics
from user.services.custom_redirects import CustomRedirect
from django.utils.encoding import smart_str, smart_bytes, DjangoUnicodeDecodeError
from rest_framework.views import APIView
from django.urls import reverse
from rest_framework.response import Response
from django.contrib.sites.shortcuts import get_current_site
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from .serializers import UserSerializer, LoginSerializer, \
    LogoutSerializer, ResetPasswordEmailSerializer, SetNewPasswordSerializer
from .services.email_sender import send_email
from django.utils.http import urlsafe_base64_decode
from .models import User


class RegisterView(APIView):
    serializer_class = UserSerializer

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response({"RequestId": str(uuid.uuid4()), "Message": "User created successfully",
                         "User": serializer.data}, status=status.HTTP_201_CREATED)


class LoginView(APIView):
    serializer_class = LoginSerializer

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            response = Response(serializer.data, status=status.HTTP_200_OK)
            response.set_cookie(key="jwt", value=serializer.data["tokens"]["access"], httponly=True)
            return response
        return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class LogoutView(APIView):
    serializer_class = LogoutSerializer
    permission_classes = (permissions.IsAuthenticated, )

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        response = Response()
        response.delete_cookie("jwt")
        response.data = {
            "message": "Success logout"
        }
        return response


class PasswordResetEmail(generics.GenericAPIView):
    serializer_class = ResetPasswordEmailSerializer

    @staticmethod
    def post(request):
        email = request.data.get("email", None)
        user = User.objects.get(email=email).first()

        if user is not None:
            ui_db64 = urlsafe_base64_decode(smart_bytes(user.id))
            token = PasswordResetTokenGenerator().make_token(user)
            current_site = get_current_site(request=request).domain
            relative_link = reverse("password-reset-confirm",
                                    kwargs={"ui_db64": ui_db64, "token": token})
            redirect_url = request.data.get("redirect_url", "")
            abs_url = 'http://' + current_site + relative_link
            email_body = "Use this link below to reset your password \n" + \
                         abs_url + "?redirect_url=" + redirect_url
            data = {
                "email_body": email_body, "to_email": user.email,
                "email_subject": "Reset your password"}
            send_email(data)

            return Response({"msg": "We have sent you a link to reset your password"},
                            status=status.HTTP_200_OK)
        return Response({"msg": "Email is not correct"}, status=status.HTTP_400_BAD_REQUEST)


class PasswordTokenCheck(generics.GenericAPIView):
    serializer_class = SetNewPasswordSerializer

    def get(self, request, ui_db64, token):
        redirect_url = request.GET.get("redirect_url")

        user_id = smart_str(urlsafe_base64_decode(ui_db64))
        user = User.objects.get(id=user_id)
        if user is not None:
            try:
                if not PasswordResetTokenGenerator().check_token(user, token):
                    if len(redirect_url) > 3:
                        return CustomRedirect(redirect_url+'?token_valid=False')
                    else:
                        return CustomRedirect(os.environ.get('FRONTEND_URL', '')+'?token_valid=False')
                if redirect_url and len(redirect_url) > 3:
                    return CustomRedirect(
                        redirect_url+'?token_valid=True&message=Credentials Valid&uidb64='+ui_db64+'&token='+token)
                else:
                    return CustomRedirect(os.environ.get('FRONTEND_URL', '')+'?token_valid=False')

            except DjangoUnicodeDecodeError as _:
                try:
                    if not PasswordResetTokenGenerator().check_token(user=user, token=token):
                        return CustomRedirect(redirect_url+'?token_valid=False')
                except UnboundLocalError as _:
                    return Response({'error': 'Token is not valid, please request a new one'},
                                    status=status.HTTP_400_BAD_REQUEST)


class SetNewPassword(generics.GenericAPIView):
    serializer_class = SetNewPasswordSerializer

    def patch(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        return Response({'success': True, 'msg': 'Password reset success'}, status=status.HTTP_200_OK)
