from setuptools import setup, find_packages

setup(
    name="Lzml_Forum",
    version="0.1",
    author="Maksym Vojtsitskiy",
    author_email="vmaksimus706@gmail.com",
    description="Forum for enthusiasts of the world of video games, anime, movies",
    url="",
    packages=find_packages(),
    python_requires='>=3.9'
)
