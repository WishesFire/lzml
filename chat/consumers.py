from djangochannelsrestframework.generics import GenericAsyncAPIConsumer
from djangochannelsrestframework.observer.generics import ObserverModelInstanceMixin, action
from djangochannelsrestframework.observer import model_observer
from djangochannelsrestframework import mixins
from channels.db import database_sync_to_async
from user.models import User
from user.serializers import UserSerializer
from .models import Room, Message
from .serializers import RoomSerializer, MessageSerializer


class UserConsumer(mixins.ListModelMixin, mixins.RetrieveModelMixin, mixins.PatchModelMixin,
                   mixins.UpdateModelMixin, mixins.CreateModelMixin, mixins.DeleteModelMixin,
                   GenericAsyncAPIConsumer):

    queryset = User.objects.all()
    serializer_class = UserSerializer


class RoomConsumer(ObserverModelInstanceMixin, GenericAsyncAPIConsumer):
    queryset = Room.objects.all()
    serializer_class = RoomSerializer
    lookup_field = "pk"

    async def disconnect(self, code):
        if hasattr(self, "room_subscribe"):
            await self.remove_user_from_room(self.room_subscribe)
            await self.notify_user()
        await super().disconnect(code)

    async def notify_user(self):
        room = await self.get_room(self.room_subscribe)
        for group in self.groups:
            await self.channel_layer.group_send(
                group,
                {
                    "type": "update_users",
                    'usuarios': await self.current_users(room)
                }
            )

    @action()
    async def join_room(self, pk):
        self.room_subscribe = pk
        await self.add_user_to_room(pk)
        await self.notify_user()

    @action()
    async def leave(self, pk):
        await self.remove_user_from_room(pk)

    @action()
    async def create_message(self, message):
        room = await self.get_room(pk=self.room_subscribe)
        await database_sync_to_async(Message.objects.create)(
            room=room,
            user=self.scope["user"],
            text=message
        )

    @action()
    async def subscribe_to_messages_in_room(self, pk):
        await self.message_activity.subscribe(room=pk)

    @model_observer(Message)
    async def message_activity(self, message):
        await self.send_json(message)

    @database_sync_to_async
    def get_room(self, pk):
        return Room.objects.get(pk=pk)

    @database_sync_to_async
    def current_users(self, room):
        return [UserSerializer(user).data for user in room.current_user.all()]

    @database_sync_to_async
    def add_user_to_room(self, pk):
        user = self.scope["user"]
        if not user.current_rooms.filter(pk=self.room_subscribe).exists():
            user.current_rooms.add(Room.objects.get(pk=pk))

    @database_sync_to_async
    def remove_user_from_room(self, room):
        user = self.scope["user"]
        user.current_rooms.remove(room)
