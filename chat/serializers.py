from .models import Room, Message
from rest_framework import serializers
from user.serializers import UserSerializer


class MessageSerializer(serializers.ModelSerializer):
    user = UserSerializer()

    class Meta:
        model = Message


class RoomSerializer(serializers.ModelSerializer):
    last_message = serializers.SerializerMethodField()
    messages = MessageSerializer(many=True, read_only=True)

    class Meta:
        model = Room
        fields = ("pk", "name", "host", "message", "current_users", "last_message", )
        read_only_fields = ("message", "last_message", )

    @staticmethod
    def get_last_message(obj):
        return MessageSerializer(obj.message.order_by("created_at").last()).data

