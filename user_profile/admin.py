from django.contrib import admin
from . import models


@admin.register(models.Profile)
class ProfileAdmin(admin.ModelAdmin):
    list_display = ("user", "display_name")


@admin.register(models.ProfileRating)
class ProfileAdmin(admin.ModelAdmin):
    list_display = ("user", "profile", "value")


@admin.register(models.ProfileSubscriber)
class ProfileAdmin(admin.ModelAdmin):
    list_display = ("user", "subscriber")


@admin.register(models.ProfileComment)
class ProfileAdmin(admin.ModelAdmin):
    list_display = ("user", "profile")

