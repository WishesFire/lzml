from django.urls import path
from .views import ProfileView, ProfileUpdateView, ProfileSubscriberView, ProfileCommentView, ProfileRatingView


app_name = 'profile'

urlpatterns = [
    path("profile/", ProfileView.as_view(), name="profile"),
    path("profile/update/", ProfileUpdateView.as_view(), name="profile_update"),
    path("profile/<int:profile_id>/subcscribe/", ProfileSubscriberView.as_view(), name="profile_subscriber"),
    path("profile/<int:profile_id>/comment/", ProfileCommentView.as_view(), name="profile_comment"),
    path("profile/<int:profile_id>/rating/", ProfileRatingView.as_view(), name="profile_rating"),
]
