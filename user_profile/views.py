from rest_framework import generics, status, permissions
from rest_framework.response import Response
from rest_framework_simplejwt.authentication import JWTAuthentication
from rest_framework.viewsets import ViewSet
from django.shortcuts import get_object_or_404
from user.models import User
from .models import ProfileSubscriber, ProfileRating, ProfileComment, Profile
from .services.profile_data import get_profile_data
from .serializers import ProfileSerializer, ProfileCommentSerializer, \
    ProfileRatingSerializer, ProfileSubscriberSerializer
from .permissions import IsOwner


class ProfileView(generics.GenericAPIView):
    serializer_class = ProfileSerializer
    permission_classes = (permissions.IsAuthenticated, )
    authentication_classes = (JWTAuthentication, )

    def get(self, request):
        result = get_profile_data(request.user)
        serializer = self.serializer_class(instance=result, many=True)
        return Response(data=serializer.data, status=status.HTTP_200_OK)


class ProfileUpdateView(generics.GenericAPIView):
    serializer_class = ProfileSerializer
    permission_classes = (permissions.IsAuthenticated, IsOwner,)
    authentication_classes = (JWTAuthentication,)

    def post(self, request):
        user = get_object_or_404(User, pk=request.user.pk)
        serializer = self.get_serializer(data=request.data)

        if serializer.is_valid():
            # Or first get profile by user id and then update
            user.profile = serializer.save()
            return Response(ProfileSerializer(user.profile))
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ProfileRatingView(generics.GenericAPIView):
    serializer_class = ProfileRatingSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, profile_id):
        profile_rating = ProfileRating.objects.filter(profile=profile_id).fisrt()
        serializer = self.serializer_class(data=profile_rating, many=True)
        return Response(data=serializer.data, status=status.HTTP_200_OK)

    def post(self, request, profile_id):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(data=serializer.data, status=status.HTTP_200_OK)
        return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ProfileSubscriberView(generics.GenericAPIView):
    serializer_class = ProfileSubscriberSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, profile_id):
        profile_subscribers = ProfileSubscriber.objects.filter(profile=profile_id).fisrt()
        serializer = self.serializer_class(data=profile_subscribers, many=True)
        return Response(data=serializer.data, status=status.HTTP_200_OK)

    def post(self, request, profile_id):
        user_subscribe = get_object_or_404(ProfileSubscriber, pk=profile_id)
        serializer = self.serializer_class(instance=user_subscribe, data=request.data)
        if serializer.is_valid():
            serializer.create(validated_data=serializer.data)
            serializer.save()
            return Response(data=serializer.data, status=status.HTTP_200_OK)
        return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def put(self, request, profile_id):
        user_subscribe = get_object_or_404(ProfileSubscriber, pk=profile_id)
        serializer = self.serializer_class(instance=user_subscribe, data=request.data)
        if serializer.is_valid():
            serializer.save()

            return Response(data=serializer.data, status=status.HTTP_200_OK)
        return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ProfileCommentView(generics.GenericAPIView):
    serializer_class = ProfileCommentSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, profile_id):
        pass
