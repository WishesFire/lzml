from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
from django.core.validators import FileExtensionValidator
from user.models import User
from .services.avatar import upload_to_profile_avatar, validate_size_image


class Profile(models.Model):
    """
    User profile
    """
    GENDER = (
        ('M', 'Man'),
        ('F', 'Woman'),
    )

    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE,
        primary_key=True,
        related_name="profile")
    display_name = models.CharField(verbose_name="username", max_length=30, blank=True, null=False)
    gender = models.CharField(max_length=1, choices=GENDER)
    avatar = models.ImageField(
        upload_to=upload_to_profile_avatar,
        blank=True,
        null=True,
        validators=[FileExtensionValidator(allowed_extensions=['jpg']), validate_size_image])
    bio = models.CharField(max_length=300, blank=True)

    def __str__(self):
        return self.display_name


class ProfileRating(models.Model):
    """
    Profile rating which user get from others users
    """
    owner_user = models.ForeignKey(User, on_delete=models.CASCADE)
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE, verbose_name="profile_rating")
    value = models.SmallIntegerField(default=0, validators=[MaxValueValidator(10), MinValueValidator(1)],
                                     verbose_name="ratings")

    def __str__(self):
        return f"{self.profile} - {self.value}"


class ProfileSubscriber(models.Model):
    """
    People who have subscribed to this profile
    """
    owner_user = models.ForeignKey(User, on_delete=models.CASCADE)
    subscriber = models.ForeignKey(Profile, on_delete=models.CASCADE)

    def __str__(self):
        return "Subscriber"


class ProfileComment(models.Model):
    """
    Comments in the profile of a specific user
    """
    owner_user = models.ForeignKey(User, on_delete=models.CASCADE)
    parent = models.ForeignKey(
        'self', verbose_name="Родитель", on_delete=models.SET_NULL, blank=True, null=True
    )
    profile = models.ForeignKey(Profile, verbose_name="profile_comment", on_delete=models.CASCADE)
    text = models.TextField(max_length=5000)
    create_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ("-create_at", )

    def __str__(self):
        return "Comment"
