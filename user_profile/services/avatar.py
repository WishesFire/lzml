from django.core.exceptions import ValidationError


def upload_to_profile_avatar(instance, tmp_image_name):
    return f'avatar/{instance.id}/{tmp_image_name}'


def validate_size_image(file_obj):
    megabyte_limit = 2
    if file_obj.size > megabyte_limit * 1024 * 1024:
        raise ValidationError(f"Maximum file size - {megabyte_limit}MB")
