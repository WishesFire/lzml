from user_profile.models import Profile, ProfileComment, ProfileRating, ProfileSubscriber
from django.db import models


def get_profile_data(user):
    profile_queryset = Profile.objects.get(user=user)
    profile_rating_queryset = ProfileRating.objects.filter(draf=False).annotate(
        rating_user=models.Count("ratings")
    )
