from rest_framework import serializers
from .models import Profile, ProfileSubscriber, ProfileRating, ProfileComment


class ProfileRatingSerializer(serializers.ModelSerializer):
    username = serializers.CharField(read_only=True)
    value = serializers.IntegerField(write_only=True)

    class Meta:
        model = ProfileRating
        fields = ("value", )

    def create(self, validated_data):
        rating = ProfileRating.objects.update_or_create(
            owner_user=validated_data.get("owner_user", None),
            profile=validated_data.get("profile", None),
            value=validated_data.get("value", None)
        )
        return rating


class ProfileSubscriberSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProfileSubscriber

    def create(self, validated_data):
        subscribe = ProfileSubscriber.objects.create(

        )
        return subscribe


class ProfileCommentSerializer(serializers.ModelSerializer):
    username = serializers.CharField(read_only=True)
    text = serializers.CharField()
    create_at = serializers.DateTimeField()

    class Meta:
        model = ProfileComment
        fields = ("owner_user", "parent", "text", "create_at")

    @property
    def user(self):
        request = self.context.get("request")
        return request.user if request else None

    @staticmethod
    def get_username(obj):
        return obj.owner_user.username


class ProfileSerializer(serializers.ModelSerializer):
    # Main Profile model
    display_name = serializers.CharField(required=False, allow_blank=True, max_length=30)
    gender = serializers.ChoiceField(choices=Profile.GENDER, required=False)
    avatar = serializers.ImageField(max_length=None, use_url=True, allow_null=True, required=False)
    bio = serializers.CharField(max_length=300)

    # Another profile models
    ratings = ProfileRatingSerializer(many=True, read_only=True, source="ratings.value")
    subscribers = ProfileSubscriberSerializer(many=True, read_only=True)
    comments = ProfileCommentSerializer(many=True, read_only=True, source="comments.text")

    class Meta:
        model = Profile
        fields = ("id", "display_name", "gender", "avatar", "bio",
                  "ratings", "subscribers", "comments")

    def get_avatar_url(self, profile):
        request = self.context.get("request")
        avatar_url = profile.avatar.url
        return request.build_absolute_uri(avatar_url)

    def update(self, instance, validated_data):
        for attr, value in validated_data.items():
            setattr(instance, attr, value)
        instance.user.save()
        instance.save()
        return instance
